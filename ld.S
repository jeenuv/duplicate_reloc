
OUTPUT_FORMAT("elf64-littleaarch64");
OUTPUT_ARCH("aarch64");
ENTRY(entry);

SECTIONS {
    . = 0x80000000;
    .text . : {
        *(.text);
    }
    .head . : {
        *(.head);
    }
    .mydata . : {
        KEEP(*(.mydata*));
    }
}
