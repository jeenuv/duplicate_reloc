
CC := aarch64-linux-gnu-gcc
OD := aarch64-linux-gnu-objdump

src   := $(if $(broken),asm.broken.S,asm.S)
src_o := $(patsubst %.S,%.o,$(src))

.PHONY: dump
dump: asm
	$(OD) -D $< | less

asm: $(src_o) ld.S
	$(CC) -T ld.S -static -nostdlib -o $@ $<

$(src_o): $(src)
	$(CC) -c -o $@ $<

.PHONY: clean
clean:
	rm -f *.o asm
